hackrf (2017.02.1-0kali1) kali-dev; urgency=medium

  * Import new upstream release

 -- Sophie Brun <sophie@freexian.com>  Fri, 31 Mar 2017 14:34:42 +0200

hackrf (2015.07.2-11) unstable; urgency=medium

  * update host code to v2015.07.2-134-gb9d333a
  * do not fail when udev is not available on freebsd (Closes: #834055)
  * use ENV{ID_SOFTWARE_RADIO}="1" in udev rules (Closes: #839816)

 -- A. Maitland Bottoms <bottoms@debian.org>  Tue, 11 Oct 2016 20:59:02 -0400

hackrf (2015.07.2-10) unstable; urgency=medium

  * fix configure debian/libhackrf0.udev rule (Closes: #823810)

 -- A. Maitland Bottoms <bottoms@debian.org>  Tue, 10 May 2016 18:53:35 -0400

hackrf (2015.07.2-9) unstable; urgency=medium

  * fix modprobe config file (Closes: #822821)

 -- A. Maitland Bottoms <bottoms@debian.org>  Thu, 28 Apr 2016 19:14:17 -0400

hackrf (2015.07.2-8) unstable; urgency=medium

  * use dh_installudev and dh_installmodules
  * fix kFreeBSD build. Thanks Steven! (Closes: #815266)

 -- A. Maitland Bottoms <bottoms@debian.org>  Tue, 26 Apr 2016 20:45:07 -0400

hackrf (2015.07.2-7) unstable; urgency=medium

  * update to v2015.07.2-79-g3f4d1a4

 -- A. Maitland Bottoms <bottoms@debian.org>  Tue, 26 Apr 2016 11:21:02 -0400

hackrf (2015.07.2-6) unstable; urgency=medium

  * install udev rules in /lib/udev/rules.d
    and other lintian cleanups.

 -- A. Maitland Bottoms <bottoms@debian.org>  Tue, 12 Jan 2016 23:42:55 -0500

hackrf (2015.07.2-5) unstable; urgency=medium

  * blacklist the v4l hackrf kernel module (Closes: #807005)
  * Install updated dev rules (jawbreaker, hackrf one, and rad1o support)

 -- A. Maitland Bottoms <bottoms@debian.org>  Sat, 09 Jan 2016 16:17:43 -0500

hackrf (2015.07.2-4) unstable; urgency=medium

  * update to v2015.07.2-10-gfa6f29d
    Comment style fixup to prevent warning in c90 complier mode
    rad1o USB idProduct cleanups

 -- A. Maitland Bottoms <bottoms@debian.org>  Sat, 09 Jan 2016 15:44:27 -0500

hackrf (2015.07.2-3) unstable; urgency=medium

  * update to v2015.07.2-6-gf83fc14

 -- A. Maitland Bottoms <bottoms@debian.org>  Wed, 25 Nov 2015 22:42:28 -0500

hackrf (2015.07.2-2) unstable; urgency=medium

  * update Vcs-Browser: to cgit URL

 -- A. Maitland Bottoms <bottoms@debian.org>  Wed, 25 Nov 2015 22:42:08 -0500

hackrf (2015.07.2-1) unstable; urgency=medium

  * New upstream tag v2015.07.2
  * install libhackrf.pc correctly (Closes: #776077)
  * add watchfile, Thanks Sophie! (Closes:  #792253)

 -- A. Maitland Bottoms <bottoms@debian.org>  Sat, 08 Aug 2015 23:14:28 -0400

hackrf (2014.08.1-0kali2) kali-dev; urgency=medium

  * Add a debian/watch file

 -- Sophie Brun <sophie@freexian.com>  Fri, 10 Jul 2015 10:45:57 +0200

hackrf (2014.08.1-0kali1) kali; urgency=medium

  * Import new upstream version v2014.08.1 
  * Update debian/control: standard version (3.9.5), Vcs-git, Vcs-Browser
  * Delete 4 patches as they are now included in upstream version
  * Amend debian/rules: *-hackrh.rules has been moved to an other directory

 -- Sophie Brun <sophie@freexian.com>  Wed, 03 Sep 2014 09:44:44 +0200

hackrf (2014.08.1-1) unstable; urgency=low

  * New upstream tag v2014.08.1

 -- A. Maitland Bottoms <bottoms@debian.org>  Tue, 02 Sep 2014 23:27:34 -0400

hackrf (2014.04.1-4) unstable; urgency=low

  * Update to v2014.04.1-33-g635d429

 -- A. Maitland Bottoms <bottoms@debian.org>  Wed, 27 Aug 2014 02:49:23 -0400

hackrf (2014.04.1-3) unstable; urgency=low

  * Update to v2014.04.1-25-gaec97e7

 -- A. Maitland Bottoms <bottoms@debian.org>  Wed, 20 Aug 2014 15:40:53 -0400

hackrf (2014.04.1-2kali2) kali; urgency=low

  * Remove older libhackrf on update

 -- Mati Aharoni <muts@kali.org>  Sat, 23 Aug 2014 10:44:19 -0400

hackrf (2014.04.1-2kali1) kali; urgency=low

  * Remove older hackrf-tools on update

 -- Mati Aharoni <muts@kali.org>  Sat, 23 Aug 2014 10:32:43 -0400

hackrf (2014.04.1-2) unstable; urgency=low

  * Update to v2014.04.1-4-g44df9d1

 -- A. Maitland Bottoms <bottoms@debian.org>  Tue, 08 Jul 2014 00:48:33 -0400

hackrf (2014.04.1-1) unstable; urgency=low

  * New upstream tag v2014.04.1

 -- A. Maitland Bottoms <bottoms@debian.org>  Fri, 02 May 2014 12:17:09 -0400

hackrf (2013.07.1.452.b7e5dca-1) unstable; urgency=low

  * Update to v2013.07.1-452-gb7e5dca

 -- A. Maitland Bottoms <bottoms@debian.org>  Sun, 13 Apr 2014 12:13:48 -0400

hackrf (2013.07.1.16.d5cebd-2~bpo70+1) wheezy-backports; urgency=low

  * Rebuild for wheezy-backports.

 -- A. Maitland Bottoms <bottoms@debian.org>  Tue, 21 Jan 2014 20:30:04 -0500

hackrf (2013.07.1.16.d5cebd-2) unstable; urgency=low

  * Use kfreebsd libusb

 -- A. Maitland Bottoms <bottoms@debian.org>  Fri, 01 Nov 2013 19:17:55 -0400

hackrf (2013.07.1.16.d5cebd-1) unstable; urgency=low

  * New upstream snapshot (Closes: #724957).
  * Match GNU Radio live distribution version

 -- A. Maitland Bottoms <bottoms@debian.org>  Sun, 29 Sep 2013 15:36:44 -0400

hackrf (0.0gitf88d206-1) unstable; urgency=low

  * New upstream git

 -- A. Maitland Bottoms <bottoms@debian.org>  Sat, 08 Jun 2013 16:28:18 -0400
