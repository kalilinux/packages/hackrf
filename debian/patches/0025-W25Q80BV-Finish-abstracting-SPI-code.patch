From 58e3465ce5a1be50c32eec5e5abf4314c9495bfe Mon Sep 17 00:00:00 2001
From: Jared Boone <jboone@earfeast.com>
Date: Sun, 9 Nov 2014 16:15:28 -0800
Subject: [PATCH 25/68] W25Q80BV: Finish abstracting SPI code.

---
 firmware/common/hackrf_core.c      |   2 -
 firmware/common/hackrf_core.h      |   2 -
 firmware/common/w25q80bv.c         |  29 +++++++---
 firmware/common/w25q80bv.h         |   7 ++-
 firmware/common/w25q80bv_drv.c     | 112 -------------------------------------
 firmware/common/w25q80bv_drv.h     |  47 ----------------
 firmware/common/w25q80bv_spi.c     | 102 +++++++++++++++++++++++++++++++++
 firmware/common/w25q80bv_spi.h     |  35 ++++++++++++
 firmware/hackrf_usb/CMakeLists.txt |   2 +-
 9 files changed, 163 insertions(+), 175 deletions(-)
 delete mode 100644 firmware/common/w25q80bv_drv.c
 delete mode 100644 firmware/common/w25q80bv_drv.h
 create mode 100644 firmware/common/w25q80bv_spi.c
 create mode 100644 firmware/common/w25q80bv_spi.h

--- a/firmware/common/hackrf_core.c
+++ b/firmware/common/hackrf_core.c
@@ -71,12 +71,10 @@
 	.transfer_gather = rffc5071_spi_transfer_gather,
 };
 
-w25q80bv_driver_t spi_flash;
 rffc5071_driver_t rffc5072 = {
 	.spi = &rffc5071_spi,
 };
 
-
 void delay(uint32_t duration)
 {
 	uint32_t i;
--- a/firmware/common/hackrf_core.h
+++ b/firmware/common/hackrf_core.h
@@ -37,7 +37,6 @@
 #include "max2837.h"
 #include "max5864.h"
 #include "rffc5071.h"
-#include "w25q80bv.h"
 
 /* hardware identification number */
 #define BOARD_ID_JELLYBEAN  0
@@ -360,7 +359,6 @@
 void delay(uint32_t duration);
 extern max2837_driver_t max2837;
 extern rffc5071_driver_t rffc5072;
-extern w25q80bv_driver_t spi_flash;
 
 extern si5351c_driver_t clock_gen;
 
--- a/firmware/common/w25q80bv.c
+++ b/firmware/common/w25q80bv.c
@@ -28,8 +28,9 @@
  */
 
 #include <stdint.h>
+
 #include "w25q80bv.h"
-#include "w25q80bv_drv.h"
+#include "w25q80bv_spi.h"
 
 #define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))
 
@@ -57,7 +58,7 @@
 	drv->num_pages = 4096U;
 	drv->num_bytes = 1048576U;
 
-	w25q80bv_hw_init(drv->hw);
+	spi_init(drv->spi);
 
 	device_id = 0;
 	while(device_id != W25Q80BV_DEVICE_ID_RES)
@@ -69,7 +70,7 @@
 uint8_t w25q80bv_get_status(w25q80bv_driver_t* const drv)
 {
 	uint8_t data[] = { W25Q80BV_READ_STATUS1, 0xFF };
-	w25q80bv_hw_transfer(drv->hw, data, ARRAY_SIZE(data));
+	spi_transfer(drv->spi, data, ARRAY_SIZE(data));
 	return data[1];
 }
 
@@ -80,7 +81,7 @@
 		W25Q80BV_DEVICE_ID,
 		0xFF, 0xFF, 0xFF, 0xFF
 	};
-	w25q80bv_hw_transfer(drv->hw, data, ARRAY_SIZE(data));
+	spi_transfer(drv->spi, data, ARRAY_SIZE(data));
 	return data[4];
 }
 
@@ -91,7 +92,7 @@
 		0xFF, 0xFF, 0xFF, 0xFF,
 		0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF
 	};
-	w25q80bv_hw_transfer(drv->hw, data, ARRAY_SIZE(data));
+	spi_transfer(drv->spi, data, ARRAY_SIZE(data));
 
 	for(size_t i=0; i<8; i++) {
 		unique_id->id_8b[i]  = data[5+i];
@@ -108,7 +109,7 @@
 	w25q80bv_wait_while_busy(drv);
 
 	uint8_t data[] = { W25Q80BV_WRITE_ENABLE };
-	w25q80bv_hw_transfer(drv->hw, data, ARRAY_SIZE(data));
+	spi_transfer(drv->spi, data, ARRAY_SIZE(data));
 }
 
 void w25q80bv_chip_erase(w25q80bv_driver_t* const drv)
@@ -125,7 +126,7 @@
 	w25q80bv_wait_while_busy(drv);
 
 	uint8_t data[] = { W25Q80BV_CHIP_ERASE };
-	w25q80bv_hw_transfer(drv->hw, data, ARRAY_SIZE(data));
+	spi_transfer(drv->spi, data, ARRAY_SIZE(data));
 }
 
 /* write up a 256 byte page or partial page */
@@ -149,12 +150,12 @@
 		addr & 0xFF
 	};
 
-	const w25q80bv_transfer_t transfers[] = {
+	const spi_transfer_t transfers[] = {
 		{ header, ARRAY_SIZE(header) },
 		{ data, len }
 	};
 
-	w25q80bv_hw_transfer_multiple(drv->hw, transfers, ARRAY_SIZE(transfers));
+	spi_transfer_gather(drv->spi, transfers, ARRAY_SIZE(transfers));
 }
 
 /* write an arbitrary number of bytes */
@@ -198,3 +199,13 @@
 		w25q80bv_page_program(drv, addr, len, data);
 	}
 }
+
+spi_t w25q80bv_spi = {
+	.init = w25q80bv_spi_init,
+	.transfer = w25q80bv_spi_transfer,
+	.transfer_gather = w25q80bv_spi_transfer_gather,
+};
+
+w25q80bv_driver_t spi_flash = {
+	.spi = &w25q80bv_spi,
+};
--- a/firmware/common/w25q80bv.h
+++ b/firmware/common/w25q80bv.h
@@ -24,9 +24,10 @@
 #ifndef __W25Q80BV_H__
 #define __W25Q80BV_H__
 
+#include <stdint.h>
 #include <stddef.h>
 
-#include "w25q80bv_drv.h"
+#include "spi.h"
 
 typedef union
 {
@@ -36,7 +37,7 @@
 } w25q80bv_unique_id_t;
 
 typedef struct {
-	w25q80bv_hw_t* hw;
+	spi_t* spi;
 	size_t page_len;
 	size_t num_pages;
 	size_t num_bytes;
@@ -48,4 +49,6 @@
 uint8_t w25q80bv_get_device_id(w25q80bv_driver_t* const drv);
 void w25q80bv_get_unique_id(w25q80bv_driver_t* const drv, w25q80bv_unique_id_t* unique_id);
 
+extern w25q80bv_driver_t spi_flash;
+
 #endif//__W25Q80BV_H__
--- a/firmware/common/w25q80bv_drv.c
+++ /dev/null
@@ -1,112 +0,0 @@
-/*
- * Copyright 2013 Michael Ossmann
- * Copyright 2013 Benjamin Vernoux
- * Copyright 2014 Jared Boone, ShareBrained Technology
- *
- * This file is part of HackRF.
- *
- * This program is free software; you can redistribute it and/or modify
- * it under the terms of the GNU General Public License as published by
- * the Free Software Foundation; either version 2, or (at your option)
- * any later version.
- *
- * This program is distributed in the hope that it will be useful,
- * but WITHOUT ANY WARRANTY; without even the implied warranty of
- * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
- * GNU General Public License for more details.
- *
- * You should have received a copy of the GNU General Public License
- * along with this program; see the file COPYING.  If not, write to
- * the Free Software Foundation, Inc., 51 Franklin Street,
- * Boston, MA 02110-1301, USA.
- */
-
-#include "w25q80bv_drv.h"
-
-#include "hackrf_core.h"
-
-#include <libopencm3/lpc43xx/ssp.h>
-#include <libopencm3/lpc43xx/scu.h>
-#include <libopencm3/lpc43xx/gpio.h>
-#include <libopencm3/lpc43xx/rgu.h>
-
-static void w25q80bv_spi_select(w25q80bv_hw_t* const hw) {
-	(void)hw;
-	gpio_clear(PORT_SSP0_SSEL, PIN_SSP0_SSEL);
-}
-
-static void w25q80bv_spi_unselect(w25q80bv_hw_t* const hw) {
-	(void)hw;
-	gpio_set(PORT_SSP0_SSEL, PIN_SSP0_SSEL);
-}
-
-static uint16_t w25q80bv_spi_transfer(w25q80bv_hw_t* const hw, const uint16_t tx_data) {
-	(void)hw;
-	return ssp_transfer(SSP0_NUM, tx_data);
-}
-
-void w25q80bv_hw_init(w25q80bv_hw_t* const hw) {
-	const uint8_t serial_clock_rate = 2;
-	const uint8_t clock_prescale_rate = 2;
-
-	/* Reset SPIFI peripheral before to Erase/Write SPIFI memory through SPI */
-	RESET_CTRL1 = RESET_CTRL1_SPIFI_RST;
-	
-	/* Init SPIFI GPIO to Normal GPIO */
-	scu_pinmux(P3_3, (SCU_SSP_IO | SCU_CONF_FUNCTION2));    // P3_3 SPIFI_SCK => SSP0_SCK
-	scu_pinmux(P3_4, (SCU_GPIO_FAST | SCU_CONF_FUNCTION0)); // P3_4 SPIFI SPIFI_SIO3 IO3 => GPIO1[14]
-	scu_pinmux(P3_5, (SCU_GPIO_FAST | SCU_CONF_FUNCTION0)); // P3_5 SPIFI SPIFI_SIO2 IO2 => GPIO1[15]
-	scu_pinmux(P3_6, (SCU_GPIO_FAST | SCU_CONF_FUNCTION0)); // P3_6 SPIFI SPIFI_MISO IO1 => GPIO0[6]
-	scu_pinmux(P3_7, (SCU_GPIO_FAST | SCU_CONF_FUNCTION4)); // P3_7 SPIFI SPIFI_MOSI IO0 => GPIO5[10]
-	scu_pinmux(P3_8, (SCU_GPIO_FAST | SCU_CONF_FUNCTION4)); // P3_8 SPIFI SPIFI_CS => GPIO5[11]
-	
-	/* configure SSP pins */
-	scu_pinmux(SCU_SSP0_MISO, (SCU_SSP_IO | SCU_CONF_FUNCTION5));
-	scu_pinmux(SCU_SSP0_MOSI, (SCU_SSP_IO | SCU_CONF_FUNCTION5));
-	scu_pinmux(SCU_SSP0_SCK,  (SCU_SSP_IO | SCU_CONF_FUNCTION2));
-
-	/* configure GPIO pins */
-	scu_pinmux(SCU_FLASH_HOLD, SCU_GPIO_FAST);
-	scu_pinmux(SCU_FLASH_WP, SCU_GPIO_FAST);
-	scu_pinmux(SCU_SSP0_SSEL, (SCU_GPIO_FAST | SCU_CONF_FUNCTION4));
-
-	/* drive SSEL, HOLD, and WP pins high */
-	gpio_set(PORT_FLASH, (PIN_FLASH_HOLD | PIN_FLASH_WP));
-	w25q80bv_spi_unselect(hw);
-
-	/* Set GPIO pins as outputs. */
-	GPIO1_DIR |= (PIN_FLASH_HOLD | PIN_FLASH_WP);
-	GPIO5_DIR |= PIN_SSP0_SSEL;
-	
-	/* initialize SSP0 */
-	ssp_init(SSP0_NUM,
-			SSP_DATA_8BITS,
-			SSP_FRAME_SPI,
-			SSP_CPOL_0_CPHA_0,
-			serial_clock_rate,
-			clock_prescale_rate,
-			SSP_MODE_NORMAL,
-			SSP_MASTER,
-			SSP_SLAVE_OUT_ENABLE);
-}
-
-void w25q80bv_hw_transfer_multiple(
-	w25q80bv_hw_t* const hw,
-	const w25q80bv_transfer_t* const transfers,
-	const size_t transfer_count
-) {
-	w25q80bv_spi_select(hw);
-	for(size_t i=0; i<transfer_count; i++) {
-		for(size_t j=0; j<transfers[i].count; j++) {
-			transfers[i].data[j] = w25q80bv_spi_transfer(hw, transfers[i].data[j]);
-		}
-	}
-	w25q80bv_spi_unselect(hw);
-}
-
-void w25q80bv_hw_transfer(w25q80bv_hw_t* const hw, uint8_t* data, const size_t count) {
-	const w25q80bv_transfer_t transfer = {
-		data, count
-	};
-	w25q80bv_hw_transfer_multiple(hw, &transfer, 1);
-}
--- a/firmware/common/w25q80bv_drv.h
+++ /dev/null
@@ -1,47 +0,0 @@
-/*
- * Copyright 2013 Michael Ossmann
- * Copyright 2013 Benjamin Vernoux
- * Copyright 2014 Jared Boone, ShareBrained Technology
- *
- * This file is part of HackRF.
- *
- * This program is free software; you can redistribute it and/or modify
- * it under the terms of the GNU General Public License as published by
- * the Free Software Foundation; either version 2, or (at your option)
- * any later version.
- *
- * This program is distributed in the hope that it will be useful,
- * but WITHOUT ANY WARRANTY; without even the implied warranty of
- * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
- * GNU General Public License for more details.
- *
- * You should have received a copy of the GNU General Public License
- * along with this program; see the file COPYING.  If not, write to
- * the Free Software Foundation, Inc., 51 Franklin Street,
- * Boston, MA 02110-1301, USA.
- */
-
-#ifndef __W25Q80BV_DRV_H__
-#define __W25Q80BV_DRV_H__
-
-#include <stdint.h>
-#include <stddef.h>
-
-typedef struct {
-	uint8_t* const data;
-	const size_t count;
-} w25q80bv_transfer_t;
-
-typedef struct {
-	/* Empty for now */
-} w25q80bv_hw_t;
-
-void w25q80bv_hw_init(w25q80bv_hw_t* const hw);
-void w25q80bv_hw_transfer(w25q80bv_hw_t* const hw, uint8_t* data, const size_t count);
-void w25q80bv_hw_transfer_multiple(
-	w25q80bv_hw_t* const hw,
-	const w25q80bv_transfer_t* const transfers,
-	const size_t transfer_count
-);
-
-#endif//__W25Q80BV_DRV_H__
--- /dev/null
+++ b/firmware/common/w25q80bv_spi.c
@@ -0,0 +1,102 @@
+/*
+ * Copyright 2013 Michael Ossmann
+ * Copyright 2013 Benjamin Vernoux
+ * Copyright 2014 Jared Boone, ShareBrained Technology
+ *
+ * This file is part of HackRF.
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License as published by
+ * the Free Software Foundation; either version 2, or (at your option)
+ * any later version.
+ *
+ * This program is distributed in the hope that it will be useful,
+ * but WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ * GNU General Public License for more details.
+ *
+ * You should have received a copy of the GNU General Public License
+ * along with this program; see the file COPYING.  If not, write to
+ * the Free Software Foundation, Inc., 51 Franklin Street,
+ * Boston, MA 02110-1301, USA.
+ */
+
+#include "w25q80bv_spi.h"
+
+#include <libopencm3/lpc43xx/gpio.h>
+#include <libopencm3/lpc43xx/rgu.h>
+#include <libopencm3/lpc43xx/scu.h>
+#include <libopencm3/lpc43xx/ssp.h>
+
+#include "hackrf_core.h"
+
+void w25q80bv_spi_init(spi_t* const spi) {
+	(void)spi;
+	
+	const uint8_t serial_clock_rate = 2;
+	const uint8_t clock_prescale_rate = 2;
+
+	/* Reset SPIFI peripheral before to Erase/Write SPIFI memory through SPI */
+	RESET_CTRL1 = RESET_CTRL1_SPIFI_RST;
+	
+	/* initialize SSP0 */
+	ssp_init(SSP0_NUM,
+			SSP_DATA_8BITS,
+			SSP_FRAME_SPI,
+			SSP_CPOL_0_CPHA_0,
+			serial_clock_rate,
+			clock_prescale_rate,
+			SSP_MODE_NORMAL,
+			SSP_MASTER,
+			SSP_SLAVE_OUT_ENABLE);
+
+	/* Init SPIFI GPIO to Normal GPIO */
+	scu_pinmux(P3_3, (SCU_SSP_IO | SCU_CONF_FUNCTION2));    // P3_3 SPIFI_SCK => SSP0_SCK
+	scu_pinmux(P3_4, (SCU_GPIO_FAST | SCU_CONF_FUNCTION0)); // P3_4 SPIFI SPIFI_SIO3 IO3 => GPIO1[14]
+	scu_pinmux(P3_5, (SCU_GPIO_FAST | SCU_CONF_FUNCTION0)); // P3_5 SPIFI SPIFI_SIO2 IO2 => GPIO1[15]
+	scu_pinmux(P3_6, (SCU_GPIO_FAST | SCU_CONF_FUNCTION0)); // P3_6 SPIFI SPIFI_MISO IO1 => GPIO0[6]
+	scu_pinmux(P3_7, (SCU_GPIO_FAST | SCU_CONF_FUNCTION4)); // P3_7 SPIFI SPIFI_MOSI IO0 => GPIO5[10]
+	scu_pinmux(P3_8, (SCU_GPIO_FAST | SCU_CONF_FUNCTION4)); // P3_8 SPIFI SPIFI_CS => GPIO5[11]
+	
+	/* configure SSP pins */
+	scu_pinmux(SCU_SSP0_MISO, (SCU_SSP_IO | SCU_CONF_FUNCTION5));
+	scu_pinmux(SCU_SSP0_MOSI, (SCU_SSP_IO | SCU_CONF_FUNCTION5));
+	scu_pinmux(SCU_SSP0_SCK,  (SCU_SSP_IO | SCU_CONF_FUNCTION2));
+
+	/* configure GPIO pins */
+	scu_pinmux(SCU_FLASH_HOLD, SCU_GPIO_FAST);
+	scu_pinmux(SCU_FLASH_WP, SCU_GPIO_FAST);
+	scu_pinmux(SCU_SSP0_SSEL, (SCU_GPIO_FAST | SCU_CONF_FUNCTION4));
+
+	/* drive SSEL, HOLD, and WP pins high */
+	gpio_set(PORT_FLASH, (PIN_FLASH_HOLD | PIN_FLASH_WP));
+	gpio_set(PORT_SSP0_SSEL, PIN_SSP0_SSEL);
+
+	/* Set GPIO pins as outputs. */
+	GPIO1_DIR |= (PIN_FLASH_HOLD | PIN_FLASH_WP);
+	GPIO5_DIR |= PIN_SSP0_SSEL;
+}
+
+void w25q80bv_spi_transfer_gather(
+	spi_t* const spi,
+	const spi_transfer_t* const transfers,
+	const size_t transfer_count
+) {
+	(void)spi;
+
+	gpio_clear(PORT_SSP0_SSEL, PIN_SSP0_SSEL);
+	for(size_t i=0; i<transfer_count; i++) {
+		uint8_t* const p = transfers[i].data;
+		for(size_t j=0; j<transfers[i].count; j++) {
+			p[j] = ssp_transfer(SSP0_NUM, p[j]);
+		}
+	}
+	gpio_set(PORT_SSP0_SSEL, PIN_SSP0_SSEL);
+}
+
+void w25q80bv_spi_transfer(spi_t* const spi, void* const data, const size_t count) {
+	const spi_transfer_t transfers[] = {
+		{ data, count },
+	};
+	w25q80bv_spi_transfer_gather(spi, transfers, 1);
+}
--- /dev/null
+++ b/firmware/common/w25q80bv_spi.h
@@ -0,0 +1,35 @@
+/*
+ * Copyright 2013 Michael Ossmann
+ * Copyright 2013 Benjamin Vernoux
+ * Copyright 2014 Jared Boone, ShareBrained Technology
+ *
+ * This file is part of HackRF.
+ *
+ * This program is free software; you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License as published by
+ * the Free Software Foundation; either version 2, or (at your option)
+ * any later version.
+ *
+ * This program is distributed in the hope that it will be useful,
+ * but WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ * GNU General Public License for more details.
+ *
+ * You should have received a copy of the GNU General Public License
+ * along with this program; see the file COPYING.  If not, write to
+ * the Free Software Foundation, Inc., 51 Franklin Street,
+ * Boston, MA 02110-1301, USA.
+ */
+
+#ifndef __W25Q80BV_SPI_H__
+#define __W25Q80BV_SPI_H__
+
+#include <stddef.h>
+
+#include "spi.h"
+
+void w25q80bv_spi_init(spi_t* const spi);
+void w25q80bv_spi_transfer_gather(spi_t* const spi, const spi_transfer_t* const transfers, const size_t transfer_count);
+void w25q80bv_spi_transfer(spi_t* const spi, void* const data, const size_t count);
+
+#endif/*__W25Q80BV_SPI_H__*/
--- a/firmware/hackrf_usb/CMakeLists.txt
+++ b/firmware/hackrf_usb/CMakeLists.txt
@@ -45,7 +45,7 @@
 	"${PATH_HACKRF_FIRMWARE_COMMON}/usb_queue.c"
 	"${PATH_HACKRF_FIRMWARE_COMMON}/fault_handler.c"
 	"${PATH_HACKRF_FIRMWARE_COMMON}/w25q80bv.c"
-	"${PATH_HACKRF_FIRMWARE_COMMON}/w25q80bv_drv.c"
+	"${PATH_HACKRF_FIRMWARE_COMMON}/w25q80bv_spi.c"
 	"${PATH_HACKRF_FIRMWARE_COMMON}/cpld_jtag.c"
 	"${PATH_HACKRF_FIRMWARE_COMMON}/xapp058/lenval.c"
 	"${PATH_HACKRF_FIRMWARE_COMMON}/xapp058/micro.c"
