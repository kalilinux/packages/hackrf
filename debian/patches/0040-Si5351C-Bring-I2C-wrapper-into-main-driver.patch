From 28d629e0993985430284e2c4625af4c45d55ac0e Mon Sep 17 00:00:00 2001
From: Jared Boone <jboone@earfeast.com>
Date: Wed, 12 Nov 2014 18:32:00 -0800
Subject: [PATCH 40/68] Si5351C: Bring I2C wrapper into main driver.

---
 firmware/common/hackrf_core.h |  2 +-
 firmware/common/si5351c.c     | 27 ++++++++++++++++++++++--
 firmware/common/si5351c.h     | 11 +++++++++-
 firmware/common/si5351c_drv.c | 48 ------------------------------------------
 firmware/common/si5351c_drv.h | 49 -------------------------------------------
 firmware/hackrf-common.cmake  |  1 -
 6 files changed, 36 insertions(+), 102 deletions(-)
 delete mode 100644 firmware/common/si5351c_drv.c
 delete mode 100644 firmware/common/si5351c_drv.h

--- a/firmware/common/hackrf_core.h
+++ b/firmware/common/hackrf_core.h
@@ -32,7 +32,7 @@
 #include <stdint.h>
 #include <stdbool.h>
 
-#include "si5351c_drv.h"
+#include "si5351c.h"
 
 #include "spi_ssp.h"
 
--- a/firmware/common/si5351c.c
+++ b/firmware/common/si5351c.c
@@ -22,10 +22,33 @@
 
 #include "si5351c.h"
 
-#include "si5351c_drv.h"
-
 enum pll_sources active_clock_source;
 
+/* write to single register */
+void si5351c_write_single(si5351c_driver_t* const drv, uint8_t reg, uint8_t val)
+{
+	const uint8_t data_tx[] = { reg, val };
+	si5351c_write(drv, data_tx, 2);
+}
+
+/* read single register */
+uint8_t si5351c_read_single(si5351c_driver_t* const drv, uint8_t reg)
+{
+	const uint8_t data_tx[] = { reg };
+	uint8_t data_rx[] = { 0x00 };
+	i2c_bus_transfer(drv->bus, drv->i2c_address, data_tx, 1, data_rx, 1);
+	return data_rx[0];
+}
+
+/*
+ * Write to one or more contiguous registers. data[0] should be the first
+ * register number, one or more values follow.
+ */
+void si5351c_write(si5351c_driver_t* const drv, const uint8_t* const data, const size_t data_count)
+{
+	i2c_bus_transfer(drv->bus, drv->i2c_address, data, data_count, NULL, 0);
+}
+
 /* Disable all CLKx outputs. */
 void si5351c_disable_all_outputs(si5351c_driver_t* const drv)
 {
--- a/firmware/common/si5351c.h
+++ b/firmware/common/si5351c.h
@@ -30,7 +30,7 @@
 
 #include <stdint.h>
 
-#include "si5351c_drv.h"
+#include "i2c_bus.h"
 
 #define SI_INTDIV(x)  (x*128-512)
 
@@ -63,6 +63,11 @@
 	PLL_SOURCE_CLKIN = 1,
 };
 
+typedef struct {
+	i2c_bus_t* const bus;
+	uint8_t i2c_address;
+} si5351c_driver_t;
+
 void si5351c_disable_all_outputs(si5351c_driver_t* const drv);
 void si5351c_disable_oeb_pin_control(si5351c_driver_t* const drv);
 void si5351c_power_down_all_clocks(si5351c_driver_t* const drv);
@@ -81,6 +86,10 @@
 void si5351c_set_clock_source(si5351c_driver_t* const drv, const enum pll_sources source);
 void si5351c_activate_best_clock_source(si5351c_driver_t* const drv);
 
+void si5351c_write_single(si5351c_driver_t* const drv, uint8_t reg, uint8_t val);
+uint8_t si5351c_read_single(si5351c_driver_t* const drv, uint8_t reg);
+void si5351c_write(si5351c_driver_t* const drv, const uint8_t* const data, const size_t data_count);
+
 #ifdef __cplusplus
 }
 #endif
--- a/firmware/common/si5351c_drv.c
+++ /dev/null
@@ -1,48 +0,0 @@
-/*
- * Copyright 2012 Michael Ossmann <mike@ossmann.com>
- * Copyright 2012 Jared Boone <jared@sharebrained.com>
- *
- * This file is part of HackRF.
- *
- * This program is free software; you can redistribute it and/or modify
- * it under the terms of the GNU General Public License as published by
- * the Free Software Foundation; either version 2, or (at your option)
- * any later version.
- *
- * This program is distributed in the hope that it will be useful,
- * but WITHOUT ANY WARRANTY; without even the implied warranty of
- * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
- * GNU General Public License for more details.
- *
- * You should have received a copy of the GNU General Public License
- * along with this program; see the file COPYING.  If not, write to
- * the Free Software Foundation, Inc., 51 Franklin Street,
- * Boston, MA 02110-1301, USA.
- */
-
-#include "si5351c_drv.h"
-
-/* write to single register */
-void si5351c_write_single(si5351c_driver_t* const drv, uint8_t reg, uint8_t val)
-{
-	const uint8_t data_tx[] = { reg, val };
-	si5351c_write(drv, data_tx, 2);
-}
-
-/* read single register */
-uint8_t si5351c_read_single(si5351c_driver_t* const drv, uint8_t reg)
-{
-	const uint8_t data_tx[] = { reg };
-	uint8_t data_rx[] = { 0x00 };
-	i2c_bus_transfer(drv->bus, drv->i2c_address, data_tx, 1, data_rx, 1);
-	return data_rx[0];
-}
-
-/*
- * Write to one or more contiguous registers. data[0] should be the first
- * register number, one or more values follow.
- */
-void si5351c_write(si5351c_driver_t* const drv, const uint8_t* const data, const size_t data_count)
-{
-	i2c_bus_transfer(drv->bus, drv->i2c_address, data, data_count, NULL, 0);
-}
--- a/firmware/common/si5351c_drv.h
+++ /dev/null
@@ -1,49 +0,0 @@
-/*
- * Copyright 2012 Michael Ossmann <mike@ossmann.com>
- * Copyright 2012 Jared Boone <jared@sharebrained.com>
- *
- * This file is part of HackRF.
- *
- * This program is free software; you can redistribute it and/or modify
- * it under the terms of the GNU General Public License as published by
- * the Free Software Foundation; either version 2, or (at your option)
- * any later version.
- *
- * This program is distributed in the hope that it will be useful,
- * but WITHOUT ANY WARRANTY; without even the implied warranty of
- * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
- * GNU General Public License for more details.
- *
- * You should have received a copy of the GNU General Public License
- * along with this program; see the file COPYING.  If not, write to
- * the Free Software Foundation, Inc., 51 Franklin Street,
- * Boston, MA 02110-1301, USA.
- */
-
-#ifndef __SI5351C_DRV_H
-#define __SI5351C_DRV_H
-
-#ifdef __cplusplus
-extern "C"
-{
-#endif
-
-#include <stdint.h>
-#include <stddef.h>
-
-#include "i2c_bus.h"
-
-typedef struct {
-	i2c_bus_t* const bus;
-	uint8_t i2c_address;
-} si5351c_driver_t;
-
-void si5351c_write_single(si5351c_driver_t* const drv, uint8_t reg, uint8_t val);
-uint8_t si5351c_read_single(si5351c_driver_t* const drv, uint8_t reg);
-void si5351c_write(si5351c_driver_t* const drv, const uint8_t* const data, const size_t data_count);
-
-#ifdef __cplusplus
-}
-#endif
-
-#endif /* __SI5351C_DRV_H */
--- a/firmware/hackrf-common.cmake
+++ b/firmware/hackrf-common.cmake
@@ -134,7 +134,6 @@
 		${PATH_HACKRF_FIRMWARE_COMMON}/sgpio.c
 		${PATH_HACKRF_FIRMWARE_COMMON}/rf_path.c
 		${PATH_HACKRF_FIRMWARE_COMMON}/si5351c.c
-		${PATH_HACKRF_FIRMWARE_COMMON}/si5351c_drv.c
 		${PATH_HACKRF_FIRMWARE_COMMON}/max2837.c
 		${PATH_HACKRF_FIRMWARE_COMMON}/max2837_target.c
 		${PATH_HACKRF_FIRMWARE_COMMON}/max5864.c
